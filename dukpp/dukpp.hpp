
// C++ header only stuff

// #define dukpp_cpp somewhere before including this file

#pragma once

#include <duktape.h>
#include <new>

struct duk final
{
	duk_context* _duk;
	operator duk_context* (void)noexcept;

	duk(const duk&) = delete;
	duk& operator=(const duk&) = delete;

	duk(void)noexcept;
	~duk(void)noexcept;

	struct ref final
	{
		ref(const ref&)noexcept;
		ref& operator=(const ref&)noexcept;

		ref(void)noexcept;

		~ref(void)noexcept;

		duk_context* _duk;

		static ref get(duk_context*, duk_idx_t = -1)noexcept;
		void set(duk_idx_t = -1)noexcept;

		void push(void)const noexcept;

		operator bool(void)const noexcept { return _duk != nullptr; }
	private:
		static void stash(const ref*, duk_idx_t idx);
	};

	class str final
	{
		duk::ref _ref;
		const char* _txt;
	public:

		str(void)noexcept { _txt = nullptr; }
		str(duk_context* duk, const char* txt)noexcept
		{
			if (txt)
				_txt = duk_push_string(duk, txt);
			else
			{
				duk_push_null(duk); _txt = nullptr;
			}
			_ref = duk::ref::get(duk);
			duk_pop(duk);
		}

		~str(void)noexcept {}

		str(const str& them)noexcept { if (_ref = them._ref) { _txt = them._txt; } }

		str& operator=(const str& them)noexcept { this->~str(); return *(new(this)duk::str(them)); }

		bool operator==(const str& them)const noexcept { return (!_ref) ? !(them._ref) : (!strcmp(_txt, them._txt)); }
		bool operator==(const char* txt)const noexcept { return !_ref ? nullptr == txt : (txt && !strcmp(_txt, txt)); }

		static str get(duk_context* duk, duk_idx_t idx = -1)noexcept { str val; val._ref = duk::ref::get(duk, idx); return val; }
		void set(duk_idx_t idx = -1)noexcept { _ref.set(idx); }

		void push(void)const noexcept { _ref.push(); }
	};
};

#ifdef dukpp_cpp

#include <assert.h>

duk::operator duk_context* (void)noexcept { return _duk; }

duk::duk(void)noexcept :
	_duk(duk_create_heap_default())
{
}

duk::~duk(void)noexcept
{
	duk_destroy_heap(_duk);
}

duk::ref::ref(void)noexcept
{
	_duk = nullptr;
}

duk::ref::ref(const ref& them)noexcept
{
	if (_duk = them._duk)
	{
		them.push();
		duk::ref::stash(this, -1);
		duk_pop(_duk);
	}
}

duk::ref::~ref(void)noexcept
{
	if (_duk)
	{
		duk_push_heap_stash(_duk);
		duk_push_pointer(_duk, (void*)this);
		duk_del_prop(_duk, -2);
		duk_pop(_duk);
	}
}

void duk::ref::stash(const duk::ref* ref, duk_idx_t idx)
{
	auto ctx = ref->_duk;

	duk_push_heap_stash(ctx);
	duk_push_pointer(ctx, (void*)ref);
	duk_dup(ctx, idx);
	duk_put_prop(ctx, -3);
	duk_pop(ctx);
}


duk::ref duk::ref::get(duk_context* ctx, duk_idx_t idx)noexcept
{
	idx = duk_normalize_index(ctx, idx);

	duk::ref ref;

	ref._duk = ctx;

	// stash
	duk::ref::stash(&ref, -1);

	return ref;
}

void duk::ref::push(void) const noexcept
{
	assert(_duk);

	duk_push_heap_stash(_duk);
	duk_push_pointer(_duk, (void*)this);
	duk_get_prop(_duk, -2);
	duk_remove(_duk, -2);
}

#endif
