

// HIDEOS idea of this - https://github.com/g-pechorin/cpp-generators

#include <iostream>

// do a test - but - only run each check once before restarting in a tailcall. obviously needs RAII to cleanup

#define TEST() void test(const size_t line); void main() {std::cout << "; starting tests" << std::endl;test(__LINE__);std::cout << "; all checks SUCCESS" << std::endl;} void test(const size_t line)

#define CHECK(CON) \
	if (line < __LINE__) \
	{ \
		if (!(CON)) \
		{ \
			std::cerr << "\nFAIL\t! " << __FILE__  << ":" << __LINE__ << "\n\t\t!" << (#CON) << "\n" << std::endl; \
			exit(EXIT_FAILURE); \
		} \
		else \
		{ \
			std::cout << "> PASSED ; " << (#CON) << std::endl; \
			test(__LINE__); \
			return; \
		} \
	} \


