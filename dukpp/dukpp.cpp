
// it's header only ... but this is here to compensate for my CMakeFoo

#ifdef dukpp_test

#define dukpp_cpp

#include "dukpp.hpp"

#include "test.hpp"

TEST()
{
	duk ctx;

	duk_push_int(ctx, 17);

	CHECK(1 == duk_get_top(ctx));

	auto val = duk::ref::get(ctx);

	CHECK(1 == duk_get_top(ctx));
}


#endif