
this is [DukTape 2.7.0](https://duktape.org/) re'hoot'ed for [fips](http://floooh.github.io/fips/)

it has the thing to make it work on Windows 7, but, that shouldn't happen on not-windows

it also include `m` in the tests which is needed for not-Windows

finally, there's a header only library of my own design.
it provides a RAII context and a RAII reference to some value, and a wrapper to do the ref as a string.
