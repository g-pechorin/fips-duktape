
cmake_minimum_required(VERSION 2.8...3.13)
project(fips-duktape)
# include the fips main cmake file
get_filename_component(FIPS_ROOT_DIR "../fips" ABSOLUTE)
include("${FIPS_ROOT_DIR}/cmake/fips.cmake")

fips_setup()

	# duktape
	fips_begin_lib(duktape)
		fips_dir(src.marked)
		fips_files(
			duk_config.h
			duktape.c
			duktape.h
		)
	fips_end_lib()
	target_include_directories(duktape PUBLIC src.marked)

	# the "original?" module approach
		fips_begin_lib(dukmod_duktape)
			fips_deps(duktape)
			fips_dir(module-duktape)
			fips_files(
				duk_module_duktape.c
				duk_module_duktape.h
			)
		fips_end_lib()
		target_include_directories(dukmod_duktape PUBLIC module-duktape/)

		fips_begin_app(dukmod_duktape_test cmdline)
			fips_deps(dukmod_duktape)
			fips_dir(module-duktape)
			fips_files(
				test.c
			)
		fips_end_app()
	
	# the "more nodejs?" approach
		fips_begin_lib(dukmod_nodeish)
			fips_deps(duktape)
			fips_dir(module-nodeish)
			fips_files(
				duk_module_node.c
				duk_module_node.h
			)
		fips_end_lib()
		target_include_directories(dukmod_nodeish PUBLIC module-nodeish/)

		fips_begin_app(dukmod_nodeish_test cmdline)
			fips_deps(dukmod_nodeish)
			fips_dir(module-nodeish)
			fips_files(
				test.c
			)
		fips_end_app()

	# add libs to not-windows
	IF (WIN32)
		# ...
	ELSE()
		target_link_libraries(dukmod_duktape_test m)
		target_link_libraries(dukmod_nodeish_test m)
	ENDIF()

	# dukpp - peter
	fips_begin_lib(dukpp)
		fips_deps(duktape)
		fips_dir(dukpp)
		fips_files(
			dukpp.hpp
			dukpp.cpp
		)
	fips_end_lib()
	target_include_directories(dukpp PUBLIC dukpp)
	fips_begin_app(dukpp_test cmdline)
		fips_deps(dukpp)
		fips_src(dukpp/ *.hpp)
		fips_src(dukpp/ *.cpp)
		target_compile_definitions(dukpp_test PUBLIC dukpp_test)
	fips_end_app()
